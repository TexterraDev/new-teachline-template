$(document).ready(function(){

  // Toggle mobile menu
  $('.hamburger').click(function() {
    $(this).toggleClass('is-active');
    $(this).siblings('.topmenu__nav').toggleClass('opened');
  });

//images lazy loading

  $('.lazy').Lazy({
    scrollDirection: 'vertical',
  	effect: "fadeIn",
    effectTime: 500,
    threshold: 0,
    visibleOnly: true
  });
  $('.pic').not('.nolazy').Lazy({
    scrollDirection: 'vertical',
  	effect: "fadeIn",
    effectTime: 500,
    threshold: 0,
    visibleOnly: true,
    afterLoad: function(element) {
      element.addClass('show');
    }
  });


  // Stick header fixed position
  var headerMenuHeight = $('.top__menu__wrap').outerHeight();

  $(window).scroll(function(){
    if ($(this).scrollTop() > 1) {
      $('.top__menu__wrap').addClass('fixed');
    } else {
      $('.top__menu__wrap').removeClass('fixed');
    }
  });
  //scroll by ankor

  $('a.toankor').click(function(e) {
      var id = $(this).attr("href");
      var offset = $(id).offset();

      $("html, body").animate({
        scrollTop: offset.top-100
      }, 700);
    });


  // Learning programm spoiler
  $('.learning__programm__block__title').click(function(){
    $(this).toggleClass('open').next().slideToggle();
  });


  // Teachers slider
  var swiper = new Swiper('.teachers__slider', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    navigation: {
      nextEl: '.teachers__slider__btn.next',
      prevEl: '.teachers__slider__btn.prev',
    },
    pagination: {
      el: '.teachers__slider__pagination',
      clickable: true,
    }
  });

  // Teachers slider
  var videoreviews = new Swiper('.videoreviews', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    navigation: {
      nextEl: '.teachers__slider__btn.next',
      prevEl: '.teachers__slider__btn.prev',
    }

  });

  // Tarifs slider
  var swiper = new Swiper('.tarifs__slider', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    touchRatio: 0,
    breakpoints: {
    // when window width is <= 999px
      999: {
        touchRatio: 1
      }
    }
  });


  $('.custom__select').selectmenu();

  // Reviews slider
  var swiper = new Swiper('.reviews__slider', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    navigation: {
      nextEl: '.reviews__slider__btn.next',
      prevEl: '.reviews__slider__btn.prev',
    },
    pagination: {
      el: '.reviews__slider__pagination',
      clickable: true,
    }
  });
  // studentworks slider
  var swiper2 = new Swiper('.studentworks__wrap', {
    slidesPerView: 3,
    spaceBetween: 20,
    navigation: {
      nextEl: '.studentworks__wrap__btn.next',
      prevEl: '.studentworks__wrap__btn.prev',
    },
    pagination: {
      el: '.studentworks__slider__pagination',
      clickable: true,
    },
    breakpoints: {
      992: {
        slidesPerView: 1
      },
      1280: {
        slidesPerView: 2
      }
    }
  });

  // Homework slider
  var swiper = new Swiper('.homework__slider', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    navigation: {
      nextEl: '.homework__slider__btn.next',
      prevEl: '.homework__slider__btn.prev',
    },
    pagination: {
      el: '.homework__slider__pagination',
      clickable: true,
    }
  });
  // logo slider
  var swiper = new Swiper('.logo__block__slider', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    loop: true,
    navigation: {
      nextEl: '.logo__block__slider__btn.next',
      prevEl: '.logo__block__slider__btn.prev',
    }
  });

  // Функция скролла и обновления значения тарифа в форме заявки
  function setSignUpTarif (tarif) { // Параметр с тарифом из переменной
    $('html, body').animate({ scrollTop: $('.signup__form__wrap').offset().top -100 }, 900); // Скроллим к форме
    $('.signup__form__options').val(tarif); // Устанавливаем тариф
    $('.signup__form__options').selectmenu("refresh"); // Обновляем selectmenu
  }

  $('.tarifs__slider__slide__link').click(function(evt) {
    evt.preventDefault();
    var btnTarif = $(this).data('tarif'); // Получаем тариф
    setSignUpTarif(btnTarif);
  });

  $('.credit__request__push').click(function(evt) {
    evt.preventDefault();
    var selectedTarif = $(this).siblings('.credit__request__options').val(); // Получаем тариф
    setSignUpTarif(selectedTarif);
    $('.signup__form__checkbox.credit').prop('checked', true);

  });

  $('.topmenu__nav__callback, .course__descr__callback').click(function(evt) {
    evt.preventDefault();
    // $('html, body').animate({ scrollTop: $('.signup__form__wrap').offset().top -100 }, 900);
    $('html, body').animate({ scrollTop: $('.tarifs__block').offset().top  }, 900);
  });

  $('.what-u-get').click(function(evt) {
    evt.preventDefault();
    $('html, body').animate({ scrollTop: $('.what__youget__wrap').offset().top -100 }, 900);
  });

  $('.about-reviews').click(function(evt) {
    evt.preventDefault();
    $('html, body').animate({ scrollTop: $('.reviews__wrap').offset().top -100 }, 900);
  });
  var canLink = $('link[rel="canonical"]').attr('href');
  console.log(canLink);
  if(canLink == 'https://teachline.ru/courses/dzenovodstvo/'){
    $('.tarifs__slider__slide__price:first-of-type, .tarifs__slider__slide__price:last-of-type').addClass('noopacity')
    if($('.tarifs__slider__slide__price').length){
      var crossTop = $('.tarifs__slider__slide__price:first-of-type').offset().top + $('.tarifs__slider__slide__price:first-of-type').height();
      var crossBottom = $('.tarifs__slider__slide__price:last-of-type').offset().top;
      var ch = crossBottom - crossTop;
      // $('.tarifs__slider__slide__price:first-of-type').append('<div class="bigcross" style="height:'+ch+'px"><svg><use xlink:href="#closecross"></svg></div>');
    }
    $('.totalsumm, .tarifs__slider__slide__price:first-of-type').addClass('zen');
  } else {
    $('.tarifs__slider__slide__price').addClass('noopacity')
  }

  var modalshown = false;

  //--up button
  $(window).scroll( function(){
      var wsup = $(window).scrollTop() - $('body').position().top;
      if (wsup > 250){
          $('#goup').animate({'opacity':1}, 0);
      } else {
          $('#goup').animate({'opacity':0}, 0);
      }
      var wsup = $(window).scrollTop() - $('#prices').position().top;

  });

  //23
  $(window).scroll( function(){
    if(modalshown === false){
      var wsup = $(window).scrollTop() - $('#prices').position().top;
      if (wsup > 0){
        showCenterModal('corona');
        modalshown = true;
      }
    }
  });


  $('#goup').click(function () {
      $('html, body').animate({scrollTop: $('body').offset().top}, 500);
  });
  //!--up button



  $('.centermodal .close').on('click', function(){
    $('#formoverlay').fadeOut(200);
    $(this).parents('.centermodal').fadeOut(200);
  });

  $('[data-form] .close').on('click', function(){
    $(this).parents('[data-form]').fadeOut(200);
    $('#formoverlay').fadeOut('200');
    $(this).parents('[data-form]').find('.field').val('');
    $(this).parents('[data-form]').find('.field').prop('disabled', false);
    $(this).parents('[data-form]').find('.field.error').removeClass('error');
  });

  
  $('#formoverlay').on('click', function () {
    $('#formoverlay').fadeOut(200);
    $('.centermodal').fadeOut(200);
  });


  $('.learnitem .toggle').on('click', function(){
    $(this).parents('.learnitem').toggleClass('active');
  });

  $('.infoshow').on('click', function(){
    $(this).parents('.priceblock').find('.informer').fadeToggle(100);
    $(this).toggleClass('active');
  });


  $('.toankor[data-promo]').on('click', function(){
    var curPromo = $(this).data('promo');
    var curModal = $(this).parents('.centermodal').data('modal');
    closeCenterModal(curModal);
    $('.signup__form__input[name="promocode"]').val(curPromo);
    $('.promobtn[data-add]').trigger('click');
  });


  $('.promobtn[data-add]').on('click', function(){
    $(this).fadeOut();
    $(this).siblings('.promobtn[data-remove]').fadeIn();
  });

  $('.promobtn[data-remove]').on('click', function(){
    $('.signup__form__input[name="promocode"]').val('');
    $(this).fadeOut();
    $(this).siblings('.promobtn[data-add]').fadeIn();
  });

  $('[name="send"] .btn').on('click', function(){
    $(this).parents('form').slideUp();
    $(this).parents('form').siblings('.paywindow').slideDown();
  });

  $('[data-chosecourse]').on('click', function(){
    var fboffset = $('#finalform');
    var thisdata = $(this).data('chosecourse');
    $('[name="send"] [name="coursetype"][value="'+thisdata+'"]').prop('checked', true);
    $("html, body").animate({
      scrollTop: fboffset.offset().top-100
    }, 700);
  });
  $('input[name="phone"]').inputmask({
  		mask: "*{1,20}[*{2,6}][*{1,2}]",
  		greedy: false,
  		placeholder: "",
  		definitions: {
  		  '*': {
  			validator: "[0-9+()\-]",
  			cardinality: 1
  		  }
  		}
  	});

  $('.lp__info__fixed').click(function(){
    $(this).toggleClass('active');
  });

});


function showCenterModal(modal){
  $('#formoverlay, [data-modal="'+modal+'"]').fadeIn(200);
};

function closeCenterModal(modal){
  $('#formoverlay, [data-modal="'+modal+'"]').fadeOut(200);
};


function thanks(){
  $('#formoverlay, [data-form="thanks"]').fadeIn(200);
};
function toggleFixedInfo () {
  $('.lp__info__fixed').addClass('active');
}

setTimeout(toggleFixedInfo, 500);


// if ($(".expert__block__image__swiper-slide").length < 2){
//   $(".swiper-button-next__expert__block, .swiper-button-prev__expert__block").fadeOut();
// }


$('.cornermodal').addClass('vis');
$(document).on('click', '.cornermodal .content',  function(){
  if($(this).parents('.cornermodal').hasClass('hide')){
    $(this).parents('.cornermodal').removeClass('hide');
  }
});
$('.cornermodal .close').on('click', function(){
  $(this).parents('.cornermodal').toggleClass('hide');
});

$('.cornermodal [data-target]').on('click', function(){
    $(this).parents('.cornermodal').removeClass('vis');
});

 var swiper = new Swiper('.expert__block__swiper-container', {
      navigation: {
        nextEl: '.swiper-button-next__expert__block',
        prevEl: '.swiper-button-prev__expert__block',
      },
      pagination: {
        el: '.expert__block__pagination',
        clickable: true,
      }
    });

function thxMessage() {
  $('#sign-up-course .container').slideUp();
  $('[thx-block]').fadeIn();
}

function showOldPrice(){
  $('.signup__form .oldprice').addClass('vis');
}
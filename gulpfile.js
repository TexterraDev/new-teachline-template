var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    rigger = require('gulp-rigger'),
    browserSync = require('browser-sync');

gulp.task('html:build', function () {
    gulp.src('app/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('sass', function(){
    return gulp.src('app/sass/main.scss')
        .pipe(sass({
          includePaths: require('node-normalize-scss').includePaths
        }))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function(){
  return gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/jquery-ui-dist/jquery-ui.js',
    'node_modules/jquery-lazy/jquery.lazy.min.js',
    'node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js',
    "node_modules/swiper/dist/js/swiper.min.js",
    "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js"
  ])
  .pipe(concat('libs.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('dist/js'));
});

gulp.task('css-libs', ['sass'], function(){
  return gulp.src([
    "node_modules/jquery-ui-dist/jquery-ui.css",
    "node_modules/jquery-ui-dist/jquery-ui.theme.css",
    "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css",
    "node_modules/swiper/dist/css/swiper.min.css",
    "node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css"
  ])
  .pipe(concat('libs.css'))
  .pipe(cssnano())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('dist/css'));
});


gulp.task('browser-sync', function(){
    browserSync({
      server: {
        baseDir: 'dist',
        index: "index.html"
      },
      notify: false
    });
});

gulp.task('watch', ['browser-sync', 'html:build', 'css-libs', 'scripts'], function(){
    gulp.watch('app/sass/**/*.scss', ['sass']);
    gulp.watch('app/templates/*.html', ['html:build'], browserSync.reload);
    gulp.watch('app/*.html',['html:build'], browserSync.reload);
    gulp.watch('dist/*.html', browserSync.reload);
    gulp.watch('dist/js/**/*.js', browserSync.reload);
});
